def get_array(count: int) -> list:
    arr = []
    for i in range(count):
        arr.append(int(input("Введите число: ")))
    return arr


def arr_sum(arr: list):
    counter = 0
    for elem in arr:
        counter += elem
    return counter


def bubble_sort(arr: list) -> list:
    for i in range(len(arr) - 1):
        for j in range(0, len(arr) - i - 1):
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]


def arr_min(arr: list) -> int:
    min = arr[0]
    for i in range(len(arr)):
        if min > arr[i]:
            min = arr[i]
    return min


def arr_max(arr: list) -> int:
    max = arr[0]
    for i in range(len(arr)):
        if max < arr[i]:
            max = arr[i]
    return max


def count_fact(count: int) -> int:
    if count > 1:
        count *= count_fact(count - 1)
    return count


def arr_fact(arr: list) -> list:
    fact = []
    for elem in arr:
        fact.append(count_fact(elem))
    return fact


if __name__ == '__main__':
    arr = get_array(int(input('Количествово элементов в массиве: ')))
    print(arr)
    array_sum = arr_sum(arr)
    print(array_sum)
    bubble_sort(arr)
    print(arr)
    array_min = arr_min(arr)
    print(array_min)
    array_max = arr_max(arr)
    print(array_max)
    factorials = arr_fact(arr)
    print(factorials)
